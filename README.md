# Projet Twitter
![](http://collectifcritique.org/IMG/png/twitter.png)
## Résumé


lien vers le git https://gitlab.com/Robinkl35/twitter_project

Ce projet a été dans le cadre scolaire, le but étant savoir ce que ressentent 
les utilisateurs de twitter sur un sujet en particulier. 

## Installation
Pour rajouter les modules : 

```bash
npm install
```

Pour démarrer le back end utliser la commande suivante : 

```bash
npm run server
```

## Outils utilisée

* Api twitter
* Mapbox
* Tone-Analyser (IBM) (https://www.ibm.com/watson/services/tone-analyzer/)
* Vuejs
* Vuetify

## Outils utilisée

Différentes function du logiciel : 
- Permet d'afficher sur une carte les tweet pour un sujet triée par
  (les tweets les plus populare ou les tweets les plus recent )
- Permet de stream les tweet
- Permet de mettre le nombre tweets que l'on veut récupérer.
- On peut cliquer sur les avatars de la carte pour voir les détails des tweets.

## Problème rencontrer

- 1% des tweet ont la geolocation des coordonnée envoie du tweet, pour pouvoir 
continuer sur une carte, si les geolocation était nul,
je prends les coordonée de l'utilisateu. 
- J'ai eut beaucoup de difficulter a récuperer le stream de tweet
(Utilisation de socket.io), le stream m'a pris enormement temps, pour résultat
bancale qui ralentie la page. 
- Problème de temps j'aurais aimer rajouter des charts pour symplifié. 
