const cors = require('cors');
const express = require('express');
var http = require('http');
var url = require('url');
const Twit = require('twit');
const dotenv = require('dotenv');
dotenv.config();


app = express();
const port2 = process.env.PORT || 3000;
const server = require('http').createServer(app);
io 	= require('socket.io')(server);
app.use(cors());
const port = 8999;

var T = new Twit({
    consumer_key:         process.env.CONSUMER_KEY,
    consumer_secret:      process.env.CONSUMER_SECRET,
    access_token:         process.env.ACCESS_TOKEN,
    access_token_secret:  process.env.ACCESS_TOKEN_SECRET
});


// function getPolitiques() {
//   T.get('users/suggestions/:slug', { slug: 'politique' }, function (err, data, response) {
//     return data;
//       });
// }

server.listen(port2);

app.route('/emotion/:text')
    .get( async function (req, res) {
// Import the IBM Watson client library

// Create a client
        var ToneAnalyzerV3 = require('watson-developer-cloud/tone-analyzer/v3');
        // Create a client
        var toneAnalyzer = new ToneAnalyzerV3(
            {
                iam_apikey: 'EXGYv4OHKilj66ybYthYLa0bDPELcux-nCW-na77SX1y', /*EXGYv4OHKilj66ybYthYLa0bDPELcux-nCW-na77SX1y*/
                version: '2017-09-21',
                url: 'https://gateway-lon.watsonplatform.net/tone-analyzer/api',
                headers: {
                    "Accept-language" :'fr'
                }
            });

        function analyse(text, language, type)
        {
            return new Promise((resolve) =>
            {
                let request = { tone_input: text,
                    content_type: type || 'text/plain',
                    content_language: language || 'fr'
                };
                toneAnalyzer.tone( request, function(err, tone)
                {
                    if(err)     resolve(err);
                    else        resolve(tone);
                });
            });
        }
        async function test(info)
        {
            let text = info[0];
            let lang = info[1];
            const splitlang = lang.split('');
            const splitText = text.split('');
            if(splitlang[lang.length - 1] === "\""){
                splitlang.pop();
                lang = splitlang.join('');
            }
            if(splitText[0] === "\""){
                splitText.shift();
                text = splitText.join('')
            }

            const tone = await analyse(text, lang);

            const result = JSON.stringify(tone, false, 1);
            return result
        }
        const tweet = JSON.stringify(req.params.text);

        const emotion = await test(tweet.split('|'));
        res.end(emotion);
    });

app.route('/api/realtime/:value')
    .get(function (req, res) {
        let value = req.params.value;
        console.log(value);
        var stream = T.stream('statuses/filter', {track: value, delimited: 10});
        io.on('connect', function(socket) { // connexion à la socket
        stream.on('tweet', function (tweet) {
            console.log(tweet);
            socket.emit('tweets', tweet);
             });
         });
         res.end("");
    });

app.route('/api/tweets/:value')
    .get(function (req, res) {
        let value = req.params.value;
        value = value.split('|');
            T.get('search/tweets', {q: value[0], result_type:value[1], count: value[2]}, function (err, data, response) {
                res.end(JSON.stringify(data))
            });
    });

app.route('/api/find/personne/:id')
    .get(function (req, res) {
        T.get('statuses/user_timeline', {user_id: req.params.id}, function (err, data, response) {
            res.end(JSON.stringify(data))
        });
    });

app.route('api/get/tweet/:id')
    .get(function (req, res) {
        T.get('statuses/show', {id: req.params.id}, function (err, data, response) {
            res.end(JSON.stringify(data))
        });
    });


app.listen(port);






