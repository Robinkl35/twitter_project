 manageEmotions =async (info, headers,emotions,tone)=>{
    const frag_info = info.value.split(' ');
    frag_info.pop();
    const value = frag_info.join(' ');
    const lang = info.lang;
    const response = await axios.get(`http://localhost:8999/emotion/${value}|${lang}`);
    // On récupère les émotions et on les affiche
    if(response.data.length !== 0 && response.data && response.data.code !== 400) {
         tone = (response.data.document_tone.tones);
        if (tone.length !== 0) {
            const t = tone[0].tone_name;
            if(!headers.includes(t)){
                headers.push(t);
                emotions.push({name: t, id: [], value:1})
            }
            else {
                const line = emotions.find(dessert => t === dessert.name);
                line.value++
            }
        }
    }   
}