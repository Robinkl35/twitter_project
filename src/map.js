intializeMap=()=>{
    mapboxgl.accessToken = 'pk.eyJ1Ijoicm9iaW5rbDMiLCJhIjoiY2pxNnAzejlzMmE2dTQ4dGRwYm5vOHMwZCJ9.aqh5xMPaJJvopSd5hEeNJw';
    const map = new mapboxgl.Map({
        container: 'map', // container id
        style: 'mapbox://styles/mapbox/dark-v9', // stylesheet location
        center: [-1.677925, 48.117668,], // starting position [lng, lat]
        zoom: 2 // starting zoom
    });
    map.addControl(new mapboxgl.NavigationControl());
    map.addControl(new mapboxgl.FullscreenControl());
    return map
}

positionOnMap=(map,info,destination,phrase)=>{

var mapboxClient = mapboxSdk({ accessToken: mapboxgl.accessToken });
mapboxClient.geocoding.forwardGeocode({ // récupère les coordonnées lattitude longitude à partir d'un lieu
    query: destination,
    autocomplete: false,
    limit: 1
}).send()
    .then(function (response) {
        if (response && response.body && response.body.features && response.body.features.length) {
            var popup = new mapboxgl.Popup({offset: 25})
                .setHTML(phrase);
            var feature = response.body.features[0];
            var geojson = {
                "type": "FeatureCollection",
                "features": [
                    {
                        "type": "Feature",
                        "properties": {
                            "message": "Foo",
                            "iconSize": [60, 60],
                            "image_url": info.url_image[0]
                        },
                        "geometry": {
                            "type": "Point",
                            "coordinates": [
                                -66.324462890625,
                                -16.024695711685304
                            ]
                        }
                    }
                ]
            };


            geojson.features.forEach(function (marker) {

                var el = document.createElement('div');
                el.className = 'marker';

                el.style.backgroundImage = `url(${marker.properties.image_url})`; // Affichage de l'image
                el.style.width = marker.properties.iconSize[0] + 'px';
                el.style.height = marker.properties.iconSize[1] + 'px';




                new mapboxgl.Marker(el)
                    .setLngLat(feature.center)
                    .setPopup(popup)
                    .addTo(map);
            });
        }
    });
}