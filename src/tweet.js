buildTweetInfo =(tweets)=>{
    const info_filter = [];
    tweets.forEach(tweet => {
        console.log(tweet);
        if(tweet.user.location && tweet.user.location.substr(0,5).toUpperCase() === 'PARIS'){
            // Ca particulier de Paris qui a des ajouts après le nom on ne garde que Paris
            tweet.user.location = 'Paris'
        }
        const tweetObject =  {user_geo:tweet.user.location,lang:[], text: [], user: [], url_image: [], user_id: [], name: [] };

        const line = info_filter.find(info => info.user_geo === tweetObject.user_geo);
        let obj = line === undefined ? tweetObject : line;
        obj.text.push(tweet.text);
        obj.user.push(tweet.user);
        tweet.metadata ? obj.lang.push(tweet.metadata.iso_language_code):obj.lang.push(tweet.user.lang);
        obj.url_image.push(tweet.user.profile_image_url);
        obj.user_id.push(tweet.user.id);
        obj.name.push(tweet.user.name);

        if( line === undefined && tweetObject.user_geo !== '' && tweetObject.user_geo.toUpperCase() !== 'FRANCE') {
                //Permet de regrouper les tweets qui ont une même localisation sur la carte
                info_filter.push(tweetObject);
            }
    });
    return info_filter
}